package com.example.a6imagelist

import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.image.view.*

class Adapter(private val contentList: List<content>) : RecyclerView.Adapter<Adapter.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.image, viewGroup, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = this.contentList.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(contentList[position])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val imageView = itemView.imageView
        private val textView = itemView.textView

        fun bind(content: content) {
            imageView.setImageResource(content.image)
            textView.text = content.text
            Log.wtf("aaaaa", content.image.toString())
        }

    }
}